
all: main_run_1 main_run_2 main_run_3 main_load_1 main_load_2 main_load_3
#main_run_4

CC=gcc

### MAIN ###

main_run_1: main_run_time.o
	${CC} -o $@ $< -L. -ldl

main_run_2: main_run_time.o libbar_d.so
	${CC} -o $@ $< -L. -lbar_d -ldl

main_run_3: main_run_time.o libqt_d.so
	${CC} -o $@ $< -L. -lqt_d -ldl

main_load_1: main_load_time.o libqt_d.so libbar_d.so
	${CC} -o $@ $< -L. -lqt_d -lbar_d

main_load_2: main_load_time.o libqt_d.so libbar_d.so
	${CC} -o $@ $< -L. -lbar_d -lqt_d

main_load_3: main_load_time.o libqt_d.so libbar_d.so
	${CC} -o $@ $< -L. -lqt_d


### O ####

%_d.o: %_d.c
	${CC} -c -fPIC $< -o $@

%.o: %.c
	${CC} -c -fPIC $< -o $@

### A ###

%.a: %.o
	ar rvcs $@ $^

### SO ###

libbar_d.so: libbar_d.o libfoo2_d.so
	${CC} -shared -Wl,-soname,$@ -o $@ $^ -L. -lfoo2_d

libqt_d.so: libqt_d.o libfoo1.a
	${CC} -shared -Wl,-soname,$@ -o $@ $< -L. -lfoo1

%.so: %.o
	${CC} -shared -Wl,-soname,$@ -o $@ $^

### CLEAN ###

clean:
	rm -f main_run_? main_load_? *.o *.so *.a *~
