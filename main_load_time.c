#include <stdio.h>

#include "libqt.h"
#include "libbar.h"

int main()
{
  printf("Qt said: %d\n", test_qt());

  printf("Bar said: %d\n", test_bar());

  printf("Qt said: %d\n", test_qt());

  printf("Bar said: %d\n", test_bar());

}
