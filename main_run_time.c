#include <stdio.h>
#include <dlfcn.h>

#include "libfoo1.h"

int main()
{
  void *handle;
  int (*fun_qt)();
  int (*fun_bar)();

  handle = dlopen("libqt_d.so", RTLD_LAZY);
  fun_qt = dlsym(handle, "test_qt");

  printf("Qt said: %d\n", (*fun_qt)());

  handle = dlopen("libbar_d.so", RTLD_LAZY);
  fun_bar = dlsym(handle, "test_bar");

  printf("Bar said: %d\n", (*fun_bar)());

  printf("Qt said: %d\n", (*fun_qt)());

  printf("Bar said: %d\n", (*fun_bar)());

  return 0;
}
